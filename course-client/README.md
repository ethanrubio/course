# Course App Client

## Getting Started
1. Install the dependencies
    ```
    yarn
    ```
1. Start the [server](../server/README.md)
1. Run the codgen to generate the GraphQL types
    ```
    yarn run codegen
    ```
1. Start the local dev client
    ```
    yarn start
    ```
