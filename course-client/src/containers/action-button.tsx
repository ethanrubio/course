import React from "react";
import { gql, useMutation } from "@apollo/client";

import Button from "../components/button";
import * as RemoveTypes from "./__generated__/remove";
import * as AddTypes from "./__generated__/add";
import { unit } from "../styles";
import { Loading } from "../components";
import { navigate } from "@reach/router";

export const ADD_USER_TO_SECTION = gql`
  mutation add($sectionID: ID!) {
    addUserToSection(sectionID: $sectionID) {
      id
      users {
        name
        email
      }
    }
  }
`;

export const REMOVE_USER_FROM_SECTION = gql`
  mutation remove($sectionID: ID!) {
    removeUserFromSection(sectionID: $sectionID) {
      id
    }
  }
`;

interface ActionButtonProps extends Partial<RemoveTypes.removeVariables> {
  nickname?: string;
  isRegistered?: boolean;
}

const RemoveFromSectionButton: React.FC<ActionButtonProps> = ({
  sectionID,
}) => {
  const [mutate, { loading, error }] = useMutation<
    RemoveTypes.remove,
    RemoveTypes.removeVariables
  >(REMOVE_USER_FROM_SECTION, {
    variables: { sectionID } as { sectionID: string },
    onError(error) {
      // use exception capturer in the future
      console.error("[error] RemoveSection ", error);
    },
    onCompleted({ removeUserFromSection }) {
      navigate("/");
    },
  });
  if (loading) return <Loading />;
  if (error) return <p>An error occurred: {error.message}</p>;

  return (
    <div>
      <Button
        style={{ marginBottom: unit * 2 }}
        onClick={() => mutate()}
        data-testid={"action-button"}
      >
        Unregister
      </Button>
    </div>
  );
};

const AddSectionButton: React.FC<ActionButtonProps> = ({
  sectionID,
  nickname,
}) => {
  const [mutate, { loading, error  }] = useMutation<
    AddTypes.add,
    AddTypes.addVariables
  >(ADD_USER_TO_SECTION, {
    variables: { sectionID } as { sectionID: string },
    onError(error) {
      // use exception capturer in the future
      console.error("[error] AddSection ", error);
    },
    onCompleted({ addUserToSection }) {
      navigate(`/section/${addUserToSection?.id}`);
    },
  });
  if (loading) return <Loading />;
  if (error) return <p>An error occurred: {error.message}</p>;

  return (
    <div>
      <Button
        style={{ marginBottom: unit * 2 }}
        onClick={() => mutate()}
        data-testid={"action-button"}
      >
        Register for {nickname}
      </Button>
    </div>
  );
};

const ActionButton: React.FC<ActionButtonProps> = ({
  isRegistered,
  sectionID,
  nickname,
}) =>
  isRegistered ? (
    <RemoveFromSectionButton sectionID={sectionID} />
  ) : (
    <AddSectionButton sectionID={sectionID} nickname={nickname} />
  );

export default ActionButton;
