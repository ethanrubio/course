import React from "react";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { Link } from "@reach/router";

import { unit } from "../styles";
import * as CourseTileTypes from "../pages/__generated__/CourseTile";
import galaxy from "../assets/images/galaxy.jpg";
import book from "../assets/images/book.jpg";

export function getBackgroundImage(name: string) {
  if (name.toLowerCase().includes("write")) {
    return `url(${book})`;
  }
  return `url(${galaxy})`;
}

interface CourseTileProps {
  course: CourseTileTypes.CourseTile;
}

const CourseTile: React.FC<CourseTileProps> = ({ course }) => {
  const { id, name, description } = course;
  return (
    <StyledLink
      to={`/course/${id}`}
      style={{
        backgroundImage: getBackgroundImage(name),
      }}
    >
      <h3>{name}</h3>
      <h5>{description}</h5>
    </StyledLink>
  );
};

export default CourseTile;

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

export const cardClassName = css({
  padding: `${unit * 4}px ${unit * 5}px`,
  borderRadius: 7,
  color: "white",
  backgroundSize: "cover",
  backgroundPosition: "center",
});

const padding = unit * 2;
const StyledLink = styled(Link)(cardClassName, {
  display: "block",
  height: 193,
  marginTop: padding,
  textDecoration: "none",
  ":not(:last-child)": {
    marginBottom: padding * 2,
  },
});
