import React from "react";
import styled from "@emotion/styled";

import { unit, colors } from "../styles";

interface HeaderProps {
  image?: string | any;
  children?: any;
}

const Header: React.FC<HeaderProps> = ({ children = "Course App" }) => {
  const email = localStorage.getItem("email") as string;

  return (
    <Container>
      <div>
        <h2>{children}</h2>
        <Subheading>{email}</Subheading>
      </div>
    </Container>
  );
};

export default Header;

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

const Container = styled("div")({
  display: "flex",
  alignItems: "center",
  marginBottom: unit * 4.5,
});

const Subheading = styled("h5")({
  marginTop: unit / 2,
  color: colors.textSecondary,
});
