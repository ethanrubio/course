import React, { Fragment } from "react";
import styled from "@emotion/styled";

import { unit } from "../styles";
import { cardClassName } from "./course-tile";
import { UserSectionSessions } from "../pages/__generated__/UserSectionSessions";
import session from "../assets/images/session.jpg";
import ActionButton from "../containers/action-button";

interface UserSessionsDetailsProps extends Partial<UserSectionSessions> {
  sectionID: string;
}

const CourseDetail: React.FC<UserSessionsDetailsProps> = ({
  user,
  userSessions,
  sectionID,
}) => (
  <Fragment>
    <h4
      style={{
        margin: "1rem auto",
      }}
    >
      Students:
    </h4>
    <ul>
      {user!
        .sections!.find((s) => s!.id === sectionID)!
        .users!.map((u) => (
          <li key={u!.email}>{u!.name}</li>
        ))}
    </ul>
    <h4
      style={{
        margin: "1rem auto",
      }}
    >
      Sessions:
    </h4>
    {userSessions!.map((s) => (
      <Card
        key={s!.id}
        style={{
          backgroundImage: `url(${session})`,
        }}
      >
        <h5 style={{ marginBottom: unit * 1 }}>
          {s!.sessionNumber} - {s!.name}
        </h5>
        <h6 style={{ marginBottom: unit * 1 }}>{s!.description}</h6>
        {new Date(s!.releaseDate) <= new Date() ? (
          <h6>Content: {s!.content.videoURL}</h6>
        ) : (
          <h6>Content to be released on {s!.releaseDate}</h6>
        )}
      </Card>
    ))}
    <ActionButton sectionID={sectionID} isRegistered={true} />
  </Fragment>
);

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

const Card = styled("div")(cardClassName, {
  height: 150,
  marginBottom: unit * 2,
});

export default CourseDetail;
