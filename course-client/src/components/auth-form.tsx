import React, { Component } from "react";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { Link } from "@reach/router";

import Button from "./button";
import { colors, unit } from "../styles";
import * as LoginTypes from "../pages/__generated__/Login";
import * as SignupTypes from "../pages/__generated__/Signup";

interface AuthFormProps {
  login?: (a: { variables: LoginTypes.LoginVariables }) => void;
  signup?: (a: { variables: SignupTypes.SignupVariables }) => void;
  errorMsg?: string;
}

interface AuthFormState {
  email: string;
  password: string;
  name?: string;
}
type LoginFormKeys = keyof AuthFormState;
export default class AuthnForm extends Component<AuthFormProps, AuthFormState> {
  state = { email: "", password: "", name: "" };

  onChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    type: LoginFormKeys
  ) => {
    const value = (event.target as HTMLInputElement).value;
    this.setState((s) => ({ ...s, ...{ [type]: value } }));
  };

  onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (this.props.login) {
      this.props.login({
        variables: { email: this.state.email, password: this.state.password },
      });
    } else if (this.props.signup) {
      this.props.signup({
        variables: {
          email: this.state.email,
          password: this.state.password,
          name: this.state.name,
        },
      });
    }
  };

  render() {
    return (
      <Container>
        <Header></Header>
        <Heading>Course App</Heading>
        <StyledForm onSubmit={(e) => this.onSubmit(e)}>
          <StyledInput
            required
            type="email"
            name="email"
            placeholder="Email"
            data-testid="login-input"
            onChange={(e) => this.onChange(e, "email")}
          />
          <StyledInput
            required
            type="password"
            name="password"
            placeholder="Password"
            data-testid="login-input"
            onChange={(e) => this.onChange(e, "password")}
          />
          {this.props.login ? null : (
            <StyledInput
              required
              type="name"
              name="name"
              placeholder="Name"
              data-testid="login-input"
              onChange={(e) => this.onChange(e, "name")}
            />
          )}
          {this.props.login ? (
            <StyledButton type="submit">Log in</StyledButton>
          ) : (
            <StyledButton type="submit">Signup</StyledButton>
          )}
          {this.props.errorMsg ? <h4>{this.props.errorMsg}</h4> : null}
          {this.props.login ? (
            <Link to={`/signup`}>Sign up instead?</Link>
          ) : (
            <Link to={`/login`}>Log in instead?</Link>
          )}
        </StyledForm>
      </Container>
    );
  }
}

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

const Container = styled("div")({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  flexGrow: 1,
  paddingBottom: unit * 6,
  color: "white",
  backgroundColor: colors.primary,
  backgroundSize: "cover",
  backgroundPosition: "center",
});

const svgClassName = css({
  display: "block",
  fill: "currentColor",
});

const Header = styled("header")(svgClassName, {
  width: "100%",
  marginBottom: unit * 5,
  padding: unit * 2.5,
  position: "relative",
});

const Heading = styled("h1")({
  margin: `${unit * 3}px 0 ${unit * 6}px`,
});

const StyledForm = styled("form")({
  width: "100%",
  maxWidth: 406,
  padding: unit * 3.5,
  borderRadius: 3,
  boxShadow: "6px 6px 1px rgba(0, 0, 0, 0.25)",
  color: colors.text,
  backgroundColor: "white",
});

const StyledInput = styled("input")({
  width: "100%",
  marginBottom: unit * 2,
  padding: `${unit * 1.25}px ${unit * 2.5}px`,
  border: `1px solid ${colors.grey}`,
  fontSize: 16,
  outline: "none",
  ":focus": {
    borderColor: colors.primary,
  },
});

const StyledButton = styled(Button)({
  marginBottom: unit * 2,
});
