import React, { Fragment } from "react";
import styled from "@emotion/styled";
import { Link } from "@reach/router";

import { unit } from "../styles";
import * as GetSectionsTypes from "../pages/__generated__/GetSections";
import classroom from "../assets/images/classroom.jpg";
import { cardClassName, getBackgroundImage } from "./course-tile";
import ActionButton from "../containers/action-button";
import { IsLoggedIn } from "../cache";
import Button from "./button";

interface SectionTileProps {
  data: GetSectionsTypes.GetSections;
}

const SectionCard = (s: GetSectionsTypes.GetSections_sections) => {
  return (
    <Fragment key={s.id}>
      <Card
        key={s.id}
        style={{
          backgroundImage: `url(${classroom})`,
        }}
      >
        <h5 style={{ marginBottom: unit * 1 }}>{s.nickname}</h5>
        <h6>{s.dateStart}</h6>
      </Card>
      {IsLoggedIn() ? (
        <ActionButton sectionID={s.id} nickname={s.nickname} />
      ) : (
        <Link to={`/login`}>
          <Button style={{
              marginBottom: unit * 2,
          }}>Login to Register</Button>
        </Link>
      )}
    </Fragment>
  );
};

const SectionTile: React.FC<SectionTileProps> = ({ data: { sections } }) => {
  const section = sections ? sections[0] : null;
  return (
    <Fragment>
      {section ? (
        <StyledLink
          key={section.courseID}
          to={`/course/${section.courseID}`}
          style={{
            backgroundImage: getBackgroundImage(section.course!.name),
          }}
        >
          <h3>{section.course!.name}</h3>
          <h5>{section.course!.description}</h5>
        </StyledLink>
      ) : null}
      {sections!.map((s) => SectionCard(s!))}
    </Fragment>
  );
};

export default SectionTile;

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

const Card = styled("div")(cardClassName, {
  height: 150,
  marginBottom: unit * 2,
});

const padding = unit * 2;
const StyledLink = styled(Link)(cardClassName, {
  display: "block",
  height: 193,
  marginTop: padding,
  textDecoration: "none",
  ":not(:last-child)": {
    marginBottom: padding * 2,
  },
});
