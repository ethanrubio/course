import React, { Fragment } from "react";
import styled from "@emotion/styled";
import { Link } from "@reach/router";

import { unit } from "../styles";
import { cardClassName, getBackgroundImage } from "./course-tile";
import { CourseDetails_course } from "../pages/__generated__/CourseDetails";
import session from "../assets/images/session.jpg";
import Button from './button';

type CourseDetailProps = Partial<CourseDetails_course>;

const CourseDetail: React.FC<CourseDetailProps> = ({
  id,
  name,
  description,
  sessions,
}) => (
  <Fragment>
    <Card
      key={id}
      style={{
        height: 193,
        backgroundImage: getBackgroundImage(name as string),
      }}
    >
      <h3>{name}</h3>
      <h5>{description}</h5>
    </Card>
    <h4
      style={{
        margin: "1rem auto",
      }}
    >
      Sessions:
    </h4>
    {sessions!.map((s) => (
      <Card
        key={s!.id}
        style={{
          backgroundImage: `url(${session})`,
        }}
      >
        <h5 style={{ marginBottom: unit * 1 }}>{s!.sessionNumber} - {s!.name}</h5>
        <h6>{s!.description}</h6>
      </Card>
    ))}

    <Link to={`/course/${id}/sections`}><Button>
      See Upcoming Courses
      </Button></Link>
  </Fragment>
);

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

const Card = styled("div")(cardClassName, {
  height: 150,
  marginBottom: unit * 2,
});


export default CourseDetail;
