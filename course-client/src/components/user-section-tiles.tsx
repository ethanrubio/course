import React, { Fragment } from "react";
import styled from "@emotion/styled";
import { Link } from "@reach/router";

import { unit } from "../styles";
import * as MyUserSections from "../pages/__generated__/MyUserSections";
import classroom from "../assets/images/classroom.jpg";
import { cardClassName, getBackgroundImage } from "./course-tile";

interface SectionTileProps {
  sections: MyUserSections.MyUserSections_user_sections[];
}

const SectionCard = (s: MyUserSections.MyUserSections_user_sections) => {
  return (
    <Fragment key={s.id}>
      <StyledLink
        key={s.id}
        to={`/section/${s.id}`}
        style={{
          backgroundImage: `url(${classroom})`,
        }}
      >
        <h5 style={{ marginBottom: unit * 1 }}>{s.nickname}</h5>
        <h6>{s.dateStart}</h6>
      </StyledLink>
    </Fragment>
  );
};

interface CoursesMap {
    [courseID: string]: MyUserSections.MyUserSections_user_sections[];
}

const CourseSectionCards = ({ courses } : { courses: CoursesMap } ) => {
 return (
    <Fragment>
        {Object.keys(courses).map((k) => {
            return (<Fragment key={k}>
                <StyledLink
                key={k}
                to={`/course/${k}`}
                style={{
                    backgroundImage: getBackgroundImage(courses[k][0].course!.name),
                }}
                >
                <h3>{courses[k][0].course!.name}</h3>
                </StyledLink>
                {courses[k]!.map((s) => SectionCard(s!))}
            </Fragment>)
        })}
    </Fragment>
  );
}

const UserSectionTile: React.FC<SectionTileProps> = ({ sections }) => {

    const courses: CoursesMap = {};
    sections.forEach((s) => {
        courses[s.courseID] = courses[s.courseID] ? [...courses[s.courseID], s] : [s];
    });
  return (
      <CourseSectionCards courses={courses} />
  );
};

export default UserSectionTile;

/**
 * STYLED COMPONENTS USED IN THIS FILE ARE BELOW HERE
 */

const padding = unit * 2;
const StyledLink = styled(Link)(cardClassName, {
  display: "block",
  height: 193,
  marginTop: padding,
  textDecoration: "none",
  ":not(:last-child)": {
    marginBottom: padding * 2,
  },
});
