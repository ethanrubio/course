import { SectionTiles, Header, Loading } from "../components";
import React, { Fragment } from "react";
import { RouteComponentProps } from "@reach/router";
import { gql, useQuery } from "@apollo/client";
import * as GetSectionsTypes from "./__generated__/GetSections";
import { COURSE_TILE_DATA } from "./courses";

export const SECTION_TILE_DATA = gql`
  fragment SectionTile on Section {
    __typename
    id
    courseID
    nickname
    dateStart
    course {
      ...CourseTile
    }
  }
  ${COURSE_TILE_DATA}
`;

export const GetSections = gql`
  query GetSections($courseID: ID!) {
    sections(courseID: $courseID) {
      ...SectionTile
    }
  }
  ${SECTION_TILE_DATA}
`;

interface SectionsProps extends RouteComponentProps {
  courseID?: string;
}

const Sections: React.FC<SectionsProps> = ({ courseID }) => {
  const { data, loading, error } = useQuery<
    GetSectionsTypes.GetSections,
    GetSectionsTypes.GetSectionsVariables
  >(GetSections, { variables: { courseID } as { courseID: string } });
  if (loading) {
    return <Loading />;
  }
  if (error) {
    return <p>ERROR</p>;
  }
  if (!data) {
    return <p>Not found</p>;
  }
  return (
    <Fragment>
      <Header />
      {data.sections && data.sections.length > 0 && (
        <SectionTiles data={data}></SectionTiles>
      )}
    </Fragment>
  );
};

export default Sections;
