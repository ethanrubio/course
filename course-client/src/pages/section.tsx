import React, { Fragment } from "react";
import { gql, useQuery } from "@apollo/client";
import { RouteComponentProps } from "@reach/router";

import { Loading, Header, SectionDetail } from "../components";
import * as UserSessionsTypes from "./__generated__/UserSectionSessions";

export const USER_SESSION_TILE_DATA = gql`
  fragment UserSessionTile on UserSession {
    __typename
    id
    courseID
    sessionNumber
    name
    description
    course {
      name
    }
    releaseDate
    content {
      videoURL
    }
  }
`;

export const USER_SECTION_DATA = gql`
  fragment UserSection on User {
    __typename
    sections {
      id
      users {
        email
        name
      }
    }
  }
`;

export const GET_USER_SESSIONS_FOR_SECTION = gql`
  query UserSectionSessions($userID: ID!, $sectionID: ID!) {
    user(id: $userID) {
      ...UserSection
    }
    userSessions(sectionID: $sectionID) {
      ...UserSessionTile
    }
  }
  ${USER_SESSION_TILE_DATA}
  ${USER_SECTION_DATA}
`;

interface SectionProps extends RouteComponentProps {
  sectionID?: string;
}

const Section: React.FC<SectionProps> = ({ sectionID }) => {
  const { data, loading, error } = useQuery<
    UserSessionsTypes.UserSectionSessions,
    UserSessionsTypes.UserSectionSessionsVariables
  >(GET_USER_SESSIONS_FOR_SECTION, {
    variables: { userID: localStorage.getItem("userId"), sectionID } as {
      userID: string;
      sectionID: string;
    },
  });

  if (loading) return <Loading />;
  if (error) {
    return <p>Error {error.message}</p>;
  }
  if (!data) return <p>Not found</p>;

  return (
    <Fragment>
      <Header />
      <SectionDetail
        sectionID={sectionID as string}
        user={data.user}
        userSessions={data.userSessions}
      />
    </Fragment>
  );
};

export default Section;
