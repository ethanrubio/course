import { Header, Loading, UserSectionTile } from "../components";
import React, { Fragment } from "react";
import { RouteComponentProps } from "@reach/router";
import { gql, useQuery } from "@apollo/client";
import * as MyUserSectionsTypes from "./__generated__/MyUserSections";

export const MY_SECTIONS_DATA = gql`
  fragment MySections on User {
    __typename
    sections {
      id
      nickname
      dateStart
      courseID
      course {
        name
      }
    }
  }
`;

export const GET_MY_USER_SECTIONS = gql`
  query MyUserSections($userID: ID!) {
    user(id: $userID) {
      ...MySections
    }
  }
  ${MY_SECTIONS_DATA}
`;

interface MySectionProps extends RouteComponentProps {}

const Section: React.FC<MySectionProps> = () => {
  const { data, loading, error } = useQuery<
    MyUserSectionsTypes.MyUserSections,
    MyUserSectionsTypes.MyUserSectionsVariables
  >(GET_MY_USER_SECTIONS, {
    variables: { userID: localStorage.getItem("userId") } as {
      userID: string;
    },
  });

  if (loading) return <Loading />;
  if (error) {
    return <p>Error {error.message}</p>;
  }
  if (!data) return <p>Not found</p>;

  return (
    <Fragment>
      <Header />
      <UserSectionTile
        sections={
          data.user!
            .sections! as MyUserSectionsTypes.MyUserSections_user_sections[]
        }
      />
    </Fragment>
  );
};

export default Section;
