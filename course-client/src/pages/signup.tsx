import React from "react";
import { gql, useMutation } from "@apollo/client";
import { navigate, RouteComponentProps } from "@reach/router";

import { AuthForm, Loading } from "../components";
import { isLoggedInVar } from "../cache";
import * as SignupTypes from "./__generated__/Signup";

export const SIGNUP_USER = gql`
  mutation Signup($email: String!, $password: String!, $name: String!) {
    signup(email: $email, password: $password, name: $name) {
      id
      token
      name
      email
      sections {
        id
      }
    }
  }
`;

interface SignupProps extends RouteComponentProps {}

const Signup: React.FC<SignupProps> = () => {
  const [signup, { loading, error }] = useMutation<
    SignupTypes.Signup,
    SignupTypes.SignupVariables
  >(SIGNUP_USER, {
    onError(error) {
      // use exception capturer in the future
      console.error("[error] Signup ", error);
    },
    onCompleted({ signup }) {
      localStorage.setItem("token", signup!.token as string);
      localStorage.setItem("email", signup!.email as string);
      localStorage.setItem("name", signup!.name as string);
      localStorage.setItem("userId", signup!.id as string);
      isLoggedInVar(true);
      // navigate to previous location
      navigate(-1);
    },
  });

  if (loading) return <Loading />;

  return <AuthForm signup={signup} errorMsg={error?.message} />;
};

export default Signup;
