import { CourseTile, Header, Loading } from "../components";
import React, { Fragment } from "react";
import { RouteComponentProps } from "@reach/router";
import { gql, useQuery } from "@apollo/client";
import * as GetCoursesTypes from "./__generated__/GetCourses";

export const SESSION_TILE_DATA = gql`
  fragment SessionTile on Session {
    __typename
    id
    courseID
    sessionNumber
    name
    description
  }
`;

export const COURSE_TILE_DATA = gql`
  fragment CourseTile on Course {
    __typename
    id
    name
    description
  }
`;

export const GET_COURSES = gql`
  query GetCourses {
    courses {
      ...CourseTile
    }
  }
  ${COURSE_TILE_DATA}
`;

interface CoursesProps extends RouteComponentProps {}

const Courses: React.FC<CoursesProps> = () => {
  const { data, loading, error } = useQuery<GetCoursesTypes.GetCourses>(
    GET_COURSES
  );

  if (loading) {
    return <Loading />;
  }
  if (error) {
    return <p>Error {error.message}</p>;
  }
  if (!data) {
    return <p>Not found</p>;
  }
  return (
    <Fragment>
      <Header />
      {data.courses &&
        data.courses.length > 0 &&
        data.courses.map((c: any) => <CourseTile key={c!.id} course={c!} />)}
    </Fragment>
  );
};

export default Courses;
