import React from "react";
import { gql, useMutation } from "@apollo/client";

import { AuthForm, Loading } from "../components";
import { isLoggedInVar } from "../cache";
import { RouteComponentProps } from "@reach/router";
import * as LoginTypes from "./__generated__/Login";
import { navigate } from "@reach/router"

export const LOGIN_USER = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      token
      email
      name
      sections {
          id
      }
    }
  }
`;

interface LoginProps extends RouteComponentProps {}

const Login: React.FC<LoginProps> = () => {
  const [login, { loading, error }] = useMutation<
    LoginTypes.Login,
    LoginTypes.LoginVariables
  >(LOGIN_USER, {
    notifyOnNetworkStatusChange: true,
    onError(error) {
      // use exception capturer in the future
      console.error("[error] Login ", error);
    },
    onCompleted({ login }) {
      localStorage.setItem("token", login!.token as string);
      localStorage.setItem("email", login!.email as string);
      localStorage.setItem("name", login!.name as string);
      localStorage.setItem("userId", login!.id as string);
      localStorage.setItem("sections", login!.id as string);
      isLoggedInVar(true);
      // navigate to previous location
      navigate(-1);
    },
  });

  if (loading) return <Loading />;

  return <AuthForm login={login} errorMsg={error?.message} />;
};

export default Login;
