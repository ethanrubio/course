import React, { Fragment, FunctionComponent } from "react";
import { Router, Redirect, RouteComponentProps } from "@reach/router";

import { Footer, PageContainer } from "../components";
import { IsLoggedIn } from "../cache";
import Course from "./course";
import Courses from "./courses";
import Sections from "./sections";
import Login from "./login";
import Signup from "./signup";
import Section from "./section";
import MySections from "./my-sections";

type Props = RouteComponentProps & {
  as: FunctionComponent;
};

const ProtectedRoute: FunctionComponent<Props> = ({
  as: Component,
  ...props
}) => {
  const { ...rest } = props;
  return IsLoggedIn() ? (
    <Component {...rest} />
  ) : (
    <Redirect from="" to="/login" noThrow />
  );
};

export { ProtectedRoute };

export default function Pages() {
  return (
    <Fragment>
      <PageContainer>
        <Router primary={false} component={Fragment}>
          <Courses path="/" />
          <Course path="/course/:courseID" />
          <ProtectedRoute path="/section/:sectionID" as={Section} />
          <ProtectedRoute path="/my-sections" as={MySections} />
          <Sections path="/course/:courseID/sections" />
          <Login path="/login"></Login>
          <Signup path="/signup"></Signup>
        </Router>
      </PageContainer>
      <Footer />
    </Fragment>
  );
}
