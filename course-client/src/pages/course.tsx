import React, { Fragment } from "react";
import { gql, useQuery } from "@apollo/client";

import { COURSE_TILE_DATA } from "./courses";
import { Loading, Header, CourseDetail } from "../components";
// import { ActionButton } from '../containers';
import { RouteComponentProps } from "@reach/router";
import * as CourseDetailsType from "./__generated__/CourseDetails";

export const SESSION_TILE_DATA = gql`
  fragment SessionTile on Session {
    __typename
    id
    courseID
    sessionNumber
    name
    description
  }
`;

export const GET_COURSE_DETAILS = gql`
  query CourseDetails($courseID: ID!) {
    course(id: $courseID) {
      sessions {
        ...SessionTile
      }
      ...CourseTile
    }
  }
  ${COURSE_TILE_DATA}
  ${SESSION_TILE_DATA}
`;

interface LaunchProps extends RouteComponentProps {
  courseID?: string;
}

const Launch: React.FC<LaunchProps> = ({ courseID }) => {
  const { data, loading, error } = useQuery<
    CourseDetailsType.CourseDetails,
    CourseDetailsType.CourseDetailsVariables
  >(GET_COURSE_DETAILS, { variables: { courseID } as { courseID: string } });

  if (loading) return <Loading />;
  if (error) {
    return <p>Error {error.message}</p>;
  }
  if (!data) return <p>Not found</p>;

  return (
    <Fragment>
      <Header />
      <CourseDetail {...(data.course as any)} />
    </Fragment>
  );
};

export default Launch;
