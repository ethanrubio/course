import { BaseModel } from "../base/model";

export interface Session extends BaseModel {
  id: string;
  course_id: string;
  session_number: number;
  name: string;
  description: string;
}

export interface UserSession extends Session, BaseModel {
  content: { [key: string]: string };
  release_date: Date;
}
