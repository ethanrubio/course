import { DatabasePoolType, sql } from "slonik";
import { BaseStore } from "../base/base";
import { Session, UserSession } from "./model";

export interface ISessionsStore {
  GetSessionsByCourse(courseID: string): Promise<readonly Session[]>;
  GetUserSessionsBySection(sectionID: string): Promise<readonly UserSession[]>;
}

export function New(db: DatabasePoolType) {
  return new SessionsStore(db);
}

class SessionsStore implements ISessionsStore, BaseStore {
  db: DatabasePoolType;
  constructor(db: DatabasePoolType) {
    this.db = db;
  }

  async GetSessionsByCourse(courseID: string) {
    // select all except the content row
    // eventually we could split out the content into a session_content table
    return (
      await this.db.query<Session>(
        sql`SELECT id, course_id, session_number, name, description FROM sessions where course_id = ${courseID}`
      )
    ).rows;
  }

  async GetUserSessionsBySection(sectionID: string) {
    return (
      await this.db.query<UserSession>(
        sql`SELECT s.*, cr.release_date FROM content_releases cr LEFT JOIN sessions s ON cr.session_id = s.id WHERE cr.section_id = ${sectionID}`
      )
    ).rows;
  }
}
