import { DatabasePoolType } from "slonik";

export interface BaseStore {
    db: DatabasePoolType;
}