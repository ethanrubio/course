import { DatabasePoolType, sql } from "slonik";
import { BaseStore } from "../base/base";
import { Course } from "./model";

export interface ICoursesStore {
  GetCourses(): Promise<readonly Course[] | null>;
  GetCourseByID(id: string): Promise<Course | null>;
}

export function New(db: DatabasePoolType) {
  return new CoursesStore(db);
}

class CoursesStore implements ICoursesStore, BaseStore {
  db: DatabasePoolType;
  constructor(db: DatabasePoolType) {
    this.db = db;
  }

  async GetCourses() {
    const q = await this.db.query<Course>(sql`SELECT * FROM courses`);
    return q.rows;
  }

  GetCourseByID(id: string) {
    return this.db.maybeOne<Course>(
      sql`SELECT * FROM courses where id = ${id}`
    );
  }
}
