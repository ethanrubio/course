import { BaseModel } from "../base/model";

export interface Course extends BaseModel {
  id: string;
  name: string;
  description: string;
}
