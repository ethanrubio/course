import { sql, DatabasePoolType } from "slonik";
import { Permission, Role } from "../../internal/auth/auth";
import { BaseStore } from "../base/base";
import { User } from "./model";

export interface IUsersStore {
  FindUserByID(id: string): Promise<User | null>;
  FindUserByEmail(email: string): Promise<User | null>;
  AddUser(
    email: string,
    password: string,
    name: string,
    roles: Role[],
    permissions: Permission[]
  ): Promise<User>;
}

export function New(db: DatabasePoolType) {
  return new UsersStore(db);
}

class UsersStore implements IUsersStore, BaseStore {
  db: DatabasePoolType;
  constructor(db: DatabasePoolType) {
    this.db = db;
  }

  FindUserByID(id: string) {
    return this.db.maybeOne<User>(sql`
        SELECT *
        FROM users
        WHERE id = ${id}
      `);
  }

  FindUserByEmail(email: string) {
    return this.db.maybeOne<User>(sql`
        SELECT *
        FROM users
        WHERE email = ${email}
      `);
  }

  async AddUser(
    email: string,
    password: string,
    name: string,
    roles: Role[],
    permissions: Permission[]
  ) {
    const meta = { roles, permissions };
    await this.db.query(
      sql`INSERT INTO users (email, password, name, meta) VALUES (${email}, ${password}, ${name}, ${sql.json(
        meta
      )})`
    );
    return this.FindUserByEmail(email);
  }
}
