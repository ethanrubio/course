import { Permission, Role } from "../../internal/auth/auth";
import { BaseModel } from "../base/model";

export interface User extends BaseModel {
    id: string,
    name: string;
    email: string;
    password: string;
    meta: {
        roles: Role[],
        permissions: Permission[],
    };
}
