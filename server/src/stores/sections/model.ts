import { BaseModel } from "../base/model";

export interface Section extends BaseModel {
  id: string
  course_id: string;
  nickname: string;
  date_start: Date;
}
