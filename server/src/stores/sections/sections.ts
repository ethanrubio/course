import { DatabasePoolType, sql } from "slonik";
import { BaseStore } from "../base/base";
import { Section } from "./model";

export interface ISectionsStore {
  GetSectionByID(id: string): Promise<Section>;
  GetSectionsByCourse(courseID: string): Promise<readonly Section[]>;
  GetSectionsByUserID(userID: string): Promise<readonly Section[]>;
  GetSectionByUserID(sectionID: string, userID: string): Promise<Section>;
  AddUserToSection(
    sectionID: string,
    userID: string
  ): Promise<Section>;
  RemoveUserFromSection(
    sectionID: string,
    userID: string
  ): Promise<Section>;
  GetUsersInSection(sectionID: string): Promise<readonly { user_id: string }[]>;
}

export function New(db: DatabasePoolType) {
  return new SectionsStore(db);
}

class SectionsStore implements ISectionsStore, BaseStore {
  db: DatabasePoolType;
  constructor(db: DatabasePoolType) {
    this.db = db;
  }

  GetSectionByID(id: string) {
    return this.db.maybeOne<Section>(
      sql`SELECT * FROM sections where id = ${id}`
    );
  }

  async GetSectionsByCourse(courseID: string) {
    return (
      await this.db.query<Section>(
        sql`SELECT * FROM sections where course_id = ${courseID}`
      )
    ).rows;
  }

  async GetSectionsByUserID(userID: string) {
    return (
      await this.db.query<Section>(
        sql`SELECT
        s.*
    FROM
        sections s
        LEFT JOIN section_users su ON s.id = su.section_id
    WHERE
        su.user_id = ${userID}`
      )
    ).rows;
  }

  GetSectionByUserID(sectionID: string, userID: string) {
    return this.db.maybeOne<Section>(
      sql`SELECT
          s.*
      FROM
          sections s
          LEFT JOIN section_users su ON s.id = su.section_id
      WHERE
          su.user_id = ${userID}
      AND su.section_id = ${sectionID}`
    );
  }

  async AddUserToSection(sectionID: string, userID: string) {
    await this.db.transaction(async (t1) => {
      // verify there are enough spaces
      const query = await t1.query<{ count: number }>(
        sql`SELECT count(*) FROM section_users WHERE section_id = ${sectionID}`
      );
      if (query.rows[0].count >= 10) {
        throw new Error("Section is at capacity.");
      }
      await t1.transaction(async (t2) => {
        // only insert if user is not in section already
        await t2.query(
          sql`INSERT INTO section_users (section_id, user_id) SELECT ${sectionID}, ${userID} WHERE NOT EXISTS (SELECT * FROM section_users WHERE section_id = ${sectionID} AND user_id = ${userID})`
        );
      });
    });
    return this.GetSectionByID(sectionID);
  }

  async RemoveUserFromSection(sectionID: string, userID: string) {
    await this.db.query(
      sql`DELETE FROM section_users WHERE section_id = ${sectionID} and user_id = ${userID}`
    );

    return this.GetSectionByID(sectionID);
  }

  async GetUsersInSection(sectionID: string) {
    return (
      await this.db.query<{ user_id: string }>(
        sql`SELECT user_id FROM section_users where section_id = ${sectionID}`
      )
    ).rows;
  }
}
