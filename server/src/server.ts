import { applyMiddleware } from "graphql-middleware";
import express from "express";
import expressJwt from "express-jwt";
import { ApolloServer, makeExecutableSchema } from "apollo-server-express";

import permissions from "./internal/middleware/permissions";
import typeDefs from "./graphql/typeDefs";
import { New as NewUsersStore } from "./stores/users/users";
import { New as NewUsersManager } from "./managers/users/users";
import { New as NewCoursesStore } from "./stores/courses/courses";
import { New as NewCoursesManager } from "./managers/courses/courses";
import { New as NewSectionsStore } from "./stores/sections/sections";
import { New as NewSectionsManager } from "./managers/sections/sections";
import { New as NewSessionsStore } from "./stores/sessions/sessions";
import { New as NewSessionsManager } from "./managers/sessions/sessions";

import { New as NewResolvers } from "./graphql/resolvers";
import { ReqWithAuth } from "./internal/auth/auth";
import { slonik } from "./internal/db/db";

const app = express();
const port = 8080;

app.use(
  expressJwt({
    secret: process.env.SECRET || "",
    algorithms: ["HS256"],
    credentialsRequired: false,
  })
);

const usersStore = NewUsersStore(slonik);
const users = NewUsersManager(usersStore);
const coursesStore = NewCoursesStore(slonik);
const courses = NewCoursesManager(coursesStore);
const sectionsStores = NewSectionsStore(slonik);
const sections = NewSectionsManager(sectionsStores, users);
const sessionsStore = NewSessionsStore(slonik);
const sessions = NewSessionsManager(sessionsStore);
const resolvers = NewResolvers({
  managers: { users, courses, sections, sessions },
});

const server = new ApolloServer({
  schema: applyMiddleware(
    makeExecutableSchema({ typeDefs, resolvers: resolvers.get() }),
    permissions
  ),
  context: ({ req }: { req: ReqWithAuth }) => {
    // when we decode the token we want to add the user to every request if there is one
    const user = req.user || null;
    return { user };
  },
});

server.applyMiddleware({ app });

app.listen({ port }, () => {
  console.log(`Course server running at http://localhost:${port}`);
});
