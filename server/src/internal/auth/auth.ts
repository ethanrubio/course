import express from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

export type Role = "student" | "admin";
export type Permission = "read:own_user" | "read:any_user";

export interface IUserAuth {
    "course-app": {
        roles: Role[];
        permissions: Permission[];
    };
    iat: number;
    exp: number;
    sub: string;
}

export interface ReqWithAuth extends express.Request {
  user: IUserAuth;
}

export function createToken(
  roles: Role[],
  permissions: Permission[],
  id: string
) {
  return jwt.sign(
    {
      "course-app": { roles, permissions },
    },
    process.env.SECRET,
    { algorithm: "HS256", subject: id, expiresIn: "1d" }
  );
}

export function validatePassword(password: string, userPassword: string) {
  return bcrypt.compare(password, userPassword);
}

export function generatePasswordHash(password: string) {
  const saltRounds = 10;
  return bcrypt.hash(password, saltRounds);
}
