import { and, or, rule, shield } from "graphql-shield";

function checkPermissions(user, permissions) {
  if (user && user["course-app"]) {
    return user["course-app"].permissions.includes(permissions);
  }
  return false;
}

const isAuthenticated = rule()((_parent, _arg, { user }) => {
  return user !== null;
});

const canReadAnyUser = rule()((_parent, _arg, { user }) => {
  return checkPermissions(user, "read:any_user");
});

const canReadOwnUser = rule()((_parent, _arg, { user }) => {
  return checkPermissions(user, "read:own_user");
});

const isReadingOwnUser = rule()((_parent, { id }, { user }) => {
  return user && user.sub === id;
});

export default shield(
  {
    Query: {
      user: or(and(canReadOwnUser, isReadingOwnUser), canReadAnyUser),
      viewer: isAuthenticated,
      userSessions: isAuthenticated,
    },
    Mutation: {
      addUserToSection: isAuthenticated,
      removeUserFromSection: isAuthenticated,
    },
  },
  { allowExternalErrors: true }
);
