import { IUsersManager } from "../managers/users/users";
import { IUserAuth } from "../internal/auth/auth";
import { ICoursesManager } from "../managers/courses/courses";
import { ISectionsManager } from "../managers/sections/sections";
import { ISessionsManager } from "../managers/sessions/sessions";
import { AuthenticationError, ForbiddenError } from "apollo-server-express";

interface Managers {
  users: IUsersManager;
  courses: ICoursesManager;
  sections: ISectionsManager;
  sessions: ISessionsManager;
}

export function New(options: { managers: Managers }) {
  return new Resolvers(options.managers);
}

class Resolvers {
  private resolvers: ReturnType<typeof createResolvers>;
  constructor(managers: Managers) {
    this.resolvers = createResolvers(managers);
  }

  get() {
    return this.resolvers;
  }
}

function createResolvers(managers: Managers) {
  const { users, courses, sessions, sections } = managers;
  return {
    Query: {
      user(_parent, { id }: { id: string }) {
        return users.FindUserByID(id);
      },
      viewer(_parent, _args, { user }: { user: IUserAuth }) {
        return users.FindUserByID(user.sub);
      },
      course(_parent, { id }: { id: string }) {
        return courses.GetCourseByID(id);
      },
      courses() {
        return courses.GetCourses();
      },
      sessions(_parent, { courseID }: { courseID: string }) {
        return sessions.GetSessionsByCourse(courseID);
      },
      sections(_parent, { courseID }: { courseID: string }) {
        return sections.GetSectionsByCourse(courseID);
      },
      async userSessions(
        _parent,
        { sectionID }: { sectionID: string },
        { user }: { user: IUserAuth }
      ) {
        if (
          !(await sections.GetSectionsByUserID(user.sub)).find(
            (s) => s.id === sectionID
          )
        ) {
          throw new ForbiddenError("User is not registered in section");
        }
        return sessions.GetUserSessionsBySection(sectionID);
      },
    },
    User: {
      async name({ id }: { id: string }) {
        const { name } = await users.FindUserByID(id);
        return name;
      },
      async email({ id }: { id: string }) {
        const { email } = await users.FindUserByID(id);
        return email;
      },
      sections({ id }: { id: string }) {
        return sections.GetSectionsByUserID(id);
      },
    },
    UserSession: {
      course({ courseID }: { courseID: string }) {
        return courses.GetCourseByID(courseID);
      },
    },
    Course: {
      sessions({ id }: { id: string }) {
        return sessions.GetSessionsByCourse(id);
      },
      sections({ id }: { id: string }) {
        return sections.GetSectionsByCourse(id);
      },
    },
    Section: {
      users({ id }: { id: string }) {
        return sections.GetUsersInSection(id);
      },
      course({ courseID }: { courseID: string }) {
        return courses.GetCourseByID(courseID);
      },
    },
    Session: {
      course({ courseID }: { courseID: string }) {
        return courses.GetCourseByID(courseID);
      },
    },
    Mutation: {
      login(_parent, { email, password }: { email: string; password: string }) {
        return users.Login(email, password);
      },
      signup(
        _parent,
        {
          email,
          password,
          name,
        }: { email: string; password: string; name: string }
      ) {
        return users.Signup(email, password, name);
      },
      addUserToSection(
        _parent,
        { sectionID }: { sectionID: string },
        { user }: { user: IUserAuth }
      ) {
        return sections.AddUserToSection(sectionID, user.sub);
      },
      removeUserFromSection(
        _parent,
        { sectionID }: { sectionID: string },
        { user }: { user: IUserAuth }
      ) {
        return sections.RemoveUserFromSection(sectionID, user.sub);
      },
    },
  };
}
