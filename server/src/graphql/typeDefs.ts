import { gql } from "apollo-server-express";

export default gql`
  scalar Date

  type User {
    id: ID!
    name: String!
    email: String!
    # users should be able to add and remove themselves from courses
    sections: [Section]
    token: String
  }

  type Session {
    id: ID!
    courseID: ID!
    sessionNumber: Int!
    name: String!
    description: String!
    course: Course!
  }

  type Content {
    videoURL: String!
  }

  type UserSession {
    id: ID!
    courseID: ID!
    sessionNumber: Int!
    name: String!
    description: String!
    course: Course!
    releaseDate: Date!
    content: Content!
  }

  type Course {
    id: ID!
    name: String!
    description: String!
    sessions: [Session]!
    sections: [Section]!
  }

  type Section {
    id: ID!
    courseID: ID!
    nickname: String!
    dateStart: Date!
    course: Course!
    # should be able to see the users signed up in the section
    users: [User]
  }

  type Query {
    user(id: ID!): User
    viewer: User!
    course(id: ID!): Course
    courses: [Course]
    sessions(courseID: ID!): [Session]
    sections(courseID: ID!): [Section]

    userSessions(sectionID: ID!): [UserSession]
  }

  type Mutation {
    # normally we wouldn't have authentication in a GraphQL schema
    # as it should be handled by a dedicated auth service
    login(email: String!, password: String!): User
    signup(email: String!, password: String! name: String!): User

    addUserToSection(sectionID: ID!): Section
    removeUserFromSection(sectionID: ID!): Section
  }
`;
