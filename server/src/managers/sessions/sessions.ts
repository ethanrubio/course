import { ISessionsStore } from "../../stores/sessions/sessions";
import { Session, UserSession } from "./model";

export interface ISessionsManager {
  GetSessionsByCourse(courseID: string): Promise<Session[]>;
  GetUserSessionsBySection(sectionID: string): Promise<UserSession[]>;
}

export function New(sessionsStore: ISessionsStore) {
  return new SessionsManager(sessionsStore);
}
class SessionsManager implements ISessionsManager {
  sessionsStore: ISessionsStore;
  constructor(sessionsStore: ISessionsStore) {
    this.sessionsStore = sessionsStore;
  }

  async GetSessionsByCourse(courseID: string) {
    return (await this.sessionsStore.GetSessionsByCourse(courseID)).map(
      (s) => ({
        id: s.id,
        courseID: s.course_id,
        sessionNumber: s.session_number,
        name: s.name,
        description: s.description,
      })
    );
  }

  async GetUserSessionsBySection(sectionID: string) {
    return (await this.sessionsStore.GetUserSessionsBySection(sectionID)).map(
      (s) => ({
        id: s.id,
        courseID: s.course_id,
        sessionNumber: s.session_number,
        name: s.name,
        description: s.description,
        content: s.content,
        releaseDate: s.release_date,
      })
    );
  }
}
