export interface Session {
  id: string;
  courseID: string;
  sessionNumber: number;
  name: string;
  description: string;
}

export interface UserSession extends Session {
  content: { [key: string]: string };
  releaseDate: Date;
}
