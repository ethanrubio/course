import { UserInputError } from "apollo-server-express";
import { ICoursesStore } from "../../stores/courses/courses";
import { Course } from "./model";

export interface ICoursesManager {
  GetCourses(): Promise<Course[]>;

  GetCourseByID(id: string): Promise<Course>;
}

export function New(coursesStore: ICoursesStore) {
    return new CoursesManager(coursesStore);
}

class CoursesManager implements ICoursesManager {
  coursesStore: ICoursesStore;
  constructor(coursesStore: ICoursesStore) {
    this.coursesStore = coursesStore;
  }

  async GetCourses() {
    const courses = await this.coursesStore.GetCourses();
    return courses.map(({ id, name, description }) => ({
      id,
      name,
      description,
    }));
  }

  async GetCourseByID(courseID: string) {
    const course = await this.coursesStore.GetCourseByID(courseID);
    if (!course) {
      throw new UserInputError("No course found with this id.");
    }

    const { id, name, description } = course;
    return {
        id,
        name,
        description
    };
  }
}
