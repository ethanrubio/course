import { ISectionsStore } from "../../stores/sections/sections";
import { Section as SectionStoreModel } from "../../stores/sections/model";
import { Section } from "./model";
import { IUsersManager } from "../users/users";
import { UserInputError } from "apollo-server-express";

export interface ISectionsManager {
  GetSectionsByCourse(courseID: string): Promise<Section[]>;
  GetSectionsByUserID(userID: string): Promise<Section[]>;
  AddUserToSection(sectionID: string, userID: string): Promise<Section>;
  RemoveUserFromSection(sectionID: string, userID: string): Promise<Section>;
  GetUsersInSection(sectionID: string): Promise<{ id }[]>;
}

export function New(
  sectionsStore: ISectionsStore,
  usersManager: IUsersManager
) {
  return new SectionsManager(sectionsStore, usersManager);
}
class SectionsManager implements ISectionsManager {
  sectionsStore: ISectionsStore;
  usersManager: IUsersManager;
  constructor(sectionsStore: ISectionsStore, usersManager: IUsersManager) {
    this.sectionsStore = sectionsStore;
    this.usersManager = usersManager;
  }

  async GetSectionsByCourse(courseID: string) {
    return (await this.sectionsStore.GetSectionsByCourse(courseID)).map(
      this.toSectionModel
    );
  }

  async GetSectionsByUserID(userID: string) {
    return (await this.sectionsStore.GetSectionsByUserID(userID)).map(
      this.toSectionModel
    );
  }

  async AddUserToSection(sectionID: string, userID: string) {
    await this.validateSectionAndUser(userID, sectionID);
    return this.toSectionModel(
      await this.sectionsStore.AddUserToSection(sectionID, userID)
    );
  }

  async RemoveUserFromSection(sectionID: string, userID: string) {
    await this.validateSectionAndUser(userID, sectionID);

    return this.toSectionModel(
      await this.sectionsStore.RemoveUserFromSection(sectionID, userID)
    );
  }

  async GetUsersInSection(sectionID: string) {
    return (await this.sectionsStore.GetUsersInSection(sectionID)).map((s) => ({
      id: s.user_id,
    }));
  }

  private async validateSectionAndUser(userID: string, sectionID: string) {
    const user = await this.usersManager.FindUserByID(userID);
    if (!user) {
      throw new UserInputError("No user found with this id.");
    }

    const section = await this.sectionsStore.GetSectionByID(sectionID);

    if (!section) {
      throw new UserInputError("No section found with this id.");
    }
  }

  toSectionModel(s: SectionStoreModel) {
    return {
      id: s.id,
      courseID: s.course_id,
      nickname: s.nickname,
      dateStart: s.date_start,
    };
  }
}
