export interface Section {
  id: string;
  courseID: string;
  nickname: string;
  dateStart: Date;
}
