import { Permission, Role } from "../../internal/auth/auth";

export interface User {
    id: string,
    name: string;
    email: string;
    roles: Role[],
    permissions: Permission[],
    token?: string;
}
