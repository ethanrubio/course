import { AuthenticationError, UserInputError } from "apollo-server-express";
import {
  createToken,
  generatePasswordHash,
  validatePassword,
} from "../../internal/auth/auth";
import { User as UserStoreModel } from "../../stores/users/model";
import { IUsersStore } from "../../stores/users/users";
import { User } from "./model";

export interface IUsersManager {
  FindUserByID(id: string): Promise<User>;
  Login(email: string, password: string): Promise<User>;
  Signup(email: string, password: string, name: string): Promise<User>;
}

export function New(userStore: IUsersStore) {
  return new UsersManager(userStore);
}

class UsersManager implements IUsersManager {
  userStore: IUsersStore;
  constructor(userStore: IUsersStore) {
    this.userStore = userStore;
  }

  async FindUserByID(id: string) {
    const user = await this.userStore.FindUserByID(id);

    if (!user) {
      throw new UserInputError("No user found with this id.");
    }

    return this.toUserModel(user);
  }

  private toUserModel(user: UserStoreModel) {
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      roles: user.meta.roles,
      permissions: user.meta.permissions,
    };
  }

  async Login(email: string, password: string) {
    const found = await this.userStore.FindUserByEmail(email);

    if (!found) {
      throw new UserInputError("No user found with this login credentials.");
    }

    const isValid = await validatePassword(password, found.password);
    if (!isValid) {
      throw new AuthenticationError("Invalid password.");
    }

    return { ...this.toUserModel(found), token: createToken(found.meta.roles, found.meta.permissions, found.id) };
  }

  async Signup(email: string, password: string, name: string) {
    const found = await this.userStore.FindUserByEmail(email);
    if (found) {
      throw new Error("User already exists");
    }

    const newUser = await this.userStore.AddUser(
      email,
      await generatePasswordHash(password),
      name,
      ["student"],
      ["read:own_user"]
    );

    return { ...this.toUserModel(newUser), token: createToken(newUser.meta.roles, newUser.meta.permissions, newUser.id) };
  }
}
