type Mock<T> = {
  [P in keyof T]?: T[P];
};

export function createMock<T>(defaults: Mock<T>) {
  return jest.fn<T, Array<Mock<T>>>().mockImplementation((mock: Mock<T>) => {
    // we need to copy this so that it doesn't reuse the mocks
    const base = { ...defaults };
    if (mock) {
      Object.keys(mock).forEach((k) => {
        base[k] = mock[k];
      });
    }
    return base as T;
  });
}
