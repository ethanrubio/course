import { ISectionsStore } from "../../../src/stores/sections/sections";
import { createMock } from "../base";

export const SectionsStoreMock = createMock<ISectionsStore>({
  GetSectionByID: jest.fn(),
  GetSectionsByCourse: jest.fn(),
  GetSectionsByUserID: jest.fn(),
  GetSectionByUserID: jest.fn(),
  AddUserToSection: jest.fn(),
  RemoveUserFromSection: jest.fn(),
  GetUsersInSection: jest.fn(),
});
