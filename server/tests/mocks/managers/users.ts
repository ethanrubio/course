import { createMock } from "../base";
import { IUsersManager } from "../../../src/managers/users/users";

export const UsersManagerMock = createMock<IUsersManager>({
  FindUserByID: jest.fn(),
  Login: jest.fn(),
  Signup: jest.fn(),
});
