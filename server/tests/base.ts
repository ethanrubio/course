interface Test<T, K, Z> {
  name: string;
  input: T;
  throwError?: Error;
  expected: K;
  shouldThrow: boolean;
  mocks?: Z;
}
