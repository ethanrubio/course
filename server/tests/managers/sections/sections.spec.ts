import { ISectionsManager, New } from "../../../src/managers/sections/sections";
import { UsersManagerMock } from "../../mocks/managers/users";
import { SectionsStoreMock } from "../../mocks/stores/sections";

describe("sections", () => {
  const tests: Test<
    Parameters<ISectionsManager["AddUserToSection"]>,
    ReturnType<ISectionsManager["AddUserToSection"]>,
    Parameters<typeof New>
  >[] = [
    {
      name: "no user found",
      input: ["sectionID", "userID"],
      expected: null,
      throwError: new Error("user not found"),
      shouldThrow: true,
      mocks: [
        new SectionsStoreMock(),
        new UsersManagerMock({
          async FindUserByID(userID: string) {
            return Promise.reject(new Error("user not found"));
          },
        }),
      ],
    },
    {
      name: "no section found",
      input: ["sectionID", "userID"],
      expected: null,
      throwError: new Error("section not found"),
      shouldThrow: true,
      mocks: [
        new SectionsStoreMock({
          async GetSectionByID(sectionID: string) {
            return Promise.reject(new Error("section not found"));
          },
        }),
        new UsersManagerMock({
          async FindUserByID(userID: string) {
            return {
              id: userID,
              email: "foo@baz.com",
              name: "foo",
              roles: [],
              permissions: [],
            };
          },
        }),
      ],
    },
    {
      name: "add user to section",
      input: ["sectionID", "userID"],
      expected: Promise.resolve({
        id: "sectionID",
        courseID: "courseID",
        nickname: "Section 1",
        dateStart: new Date("11-20-2020"),
      }),
      shouldThrow: false,
      mocks: [
        new SectionsStoreMock({
          async AddUserToSection(sectionID: string, userID: string) {
            return {
              id: sectionID,
              course_id: "courseID",
              nickname: "Section 1",
              date_start: new Date("11-20-2020"),
              created_at: new Date(),
              updated_at: new Date(),
            };
          },
          async GetSectionByID(sectionID: string) {
            return {
              id: sectionID,
              course_id: "courseID",
              nickname: "nickname",
              date_start: new Date(),
              created_at: new Date(),
              updated_at: new Date(),
            };
          },
        }),
        new UsersManagerMock({
          async FindUserByID(userID: string) {
            return {
              id: userID,
              email: "foo@baz.com",
              name: "foo",
              roles: [],
              permissions: [],
            };
          },
        }),
      ],
    },
  ];
  test.each(tests)("AddUserToSection %p", async (t) => {
    const sectionManager = New(t.mocks[0], t.mocks[1]);
    if (t.shouldThrow) {
      await expect(sectionManager.AddUserToSection(...t.input)).rejects.toThrow(
        t.throwError
      );
    } else {
      expect(await sectionManager.AddUserToSection(...t.input)).toEqual(
        await t.expected
      );
    }
  });
});
