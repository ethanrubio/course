--sections (up)
CREATE TABLE IF NOT EXISTS sections (
    id uuid DEFAULT uuid_generate_v4 () NOT NULL,
    course_id uuid NOT NULL,
    date_start date NOT NULL,
    nickname varchar(255) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_section_course_id PRIMARY KEY (id, course_id)
);

CREATE TRIGGER set_sections_timestamp
    BEFORE UPDATE ON sections
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp ();

