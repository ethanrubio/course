--initial (up)
INSERT INTO
  courses (name, description)
VALUES
  (
    'How To Write a Book',
    'Ever wondered how you could write a book? Well now is your chance! Take this course to learn how to become the next Toni Morrison.'
  ),
  (
    'The Physics of Time',
    'Time is just a construct - or is it? Take this course to dive deep into the space-time continuum.'
  );

INSERT INTO
  sessions (
    course_id,
    session_number,
    name,
    description,
    content
  )
VALUES
  (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 1, 'Decide on a topic', E'Time to brainstorm on that idea you\'ve been wanting to write about for ages. What will you say? Do you have enough to write about? Let\'s find out!', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 2, 'Write your first chapter', E'Now that you\'ve decided what you\'ll write about, it\'s time dive in and knock that first chapter out! They say that once you\'ve jumped the FIRST hurdle OF writing the FIRST chapter, the rest will come naturally!', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 3, 'Decide on a title', E'Whether you want it to be clever, hard-hitting, or mysterious, the title of your book is the first thing people will see of your book. We\'ll spend this lesson ideating on titles that make an impact.', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 4, 'Finding an Editor', E'We\'ll wrap up this course by setting you up for success and finding an editor to for you to continue to work with as your writing journey continues.', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 1, 'What is Space-Time?', E'Is it space? Is it time? Why are these 2 words now linked together? Let\'s dive into the mystery!', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 2, 'What is a Light-Year?', E'Have you ever wondered how we see stars so brightly, but can never visit them? We\'ll introduce the concept of light-years and why objects in mirror are WAY farther than they appear.', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 3, 'Wormholes - not the ones in your garden', 'Are there other universes out there? Are they using the same time we are? What is their construct of time? How do wormholes play into this?', '{ "videoURL": "placeholder" }'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 4, 'Stargazing!', E'We\'ll wrap up this course by going on a stargazing adventure and talking about the expansion of space. Where is space expanding to? Exactly', '{ "videoURL": "placeholder" }'
  );

INSERT INTO
  sections (course_id, nickname, date_start)
VALUES
  (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 'Section 1', '2020-10-30'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 'Section 2', '2020-11-13'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'How To Write a Book'
      LIMIT
        1
    ), 'Section 3', '2020-11-27'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 'Section 1', '2020-10-15'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 'Section 2', '2020-10-29'
  ), (
    (
      SELECT
        id
      FROM
        courses
      WHERE
        name = 'The Physics of Time'
      LIMIT
        1
    ), 'Section 3', '2020-11-12'
  );

-- add all content releases for each section

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '1 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'The Physics of Time'
					LIMIT 1)
				AND session_number = 1
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'The Physics of Time'
			LIMIT 1)
	);

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '2 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'The Physics of Time'
					LIMIT 1)
				AND session_number = 2
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'The Physics of Time'
			LIMIT 1)
	);

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '3 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'The Physics of Time'
					LIMIT 1)
				AND session_number = 3
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'The Physics of Time'
			LIMIT 1)
	);

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '4 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'The Physics of Time'
					LIMIT 1)
				AND session_number = 4
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'The Physics of Time'
			LIMIT 1)
	);

--

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '1 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'How To Write a Book'
					LIMIT 1)
				AND session_number = 1
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'How To Write a Book'
			LIMIT 1)
	);

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '2 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'How To Write a Book'
					LIMIT 1)
				AND session_number = 2
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'How To Write a Book'
			LIMIT 1)
	);

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '3 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'How To Write a Book'
					LIMIT 1)
				AND session_number = 3
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'How To Write a Book'
			LIMIT 1)
	);

INSERT INTO content_releases (section_id, release_date, session_id) (
	SELECT
		id,
		date_start + INTERVAL '4 week' AS release_date,
		(
			SELECT
				id
			FROM
				sessions
			WHERE
				course_id = (
					SELECT
						id
					FROM
						courses
					WHERE
						name = 'How To Write a Book'
					LIMIT 1)
				AND session_number = 4
			LIMIT 1) AS session_id
	FROM
		sections
	WHERE
		course_id = (
			SELECT
				id
			FROM
				courses
			WHERE
				name = 'How To Write a Book'
			LIMIT 1)
	);