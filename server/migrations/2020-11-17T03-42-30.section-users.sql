--section-users (up)
CREATE TABLE IF NOT EXISTS section_users (
    id bigserial NOT NULL,
    section_id uuid NOT NULL,
    user_id uuid NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_section_id_user_id PRIMARY KEY (section_id, user_id)
);

CREATE TRIGGER set_section_users_timestamp
    BEFORE UPDATE ON section_users
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp ();
