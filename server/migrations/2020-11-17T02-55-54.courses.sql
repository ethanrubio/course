--courses (up)
CREATE TABLE IF NOT EXISTS courses (
    id uuid DEFAULT uuid_generate_v4 () NOT NULL,
    name varchar(255) NOT NULL,
    description text NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_course_id PRIMARY KEY (id)
);

CREATE TRIGGER set_courses_timestamp
    BEFORE UPDATE ON courses
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp ();

