--sessions (up)
CREATE TABLE IF NOT EXISTS sessions (
    id uuid DEFAULT uuid_generate_v4 () NOT NULL,
    course_id uuid NOT NULL,
    session_number bigint NOT NULL,
    name varchar(255) NOT NULL,
    description text NOT NULL,
    content jsonb NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_session_course_id PRIMARY KEY (id, course_id)
);

CREATE TRIGGER set_sessions_timestamp
    BEFORE UPDATE ON sessions
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp ();

