--users (down)
DROP TRIGGER IF EXISTS set_users_timestamp ON users;

DROP TABLE IF EXISTS users;