--courses (down)
DROP TRIGGER IF EXISTS set_courses_timestamp ON courses;

DROP TABLE IF EXISTS courses;