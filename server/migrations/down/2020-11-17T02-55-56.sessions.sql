--sessions (down)
DROP TRIGGER IF EXISTS set_sessions_timestamp ON sessions;

DROP TABLE IF EXISTS sessions;