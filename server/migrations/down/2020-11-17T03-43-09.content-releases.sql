--content-releases (down)
DROP TRIGGER IF EXISTS set_content_releases_timestamp ON content_releases;

DROP TABLE IF EXISTS content_releases;