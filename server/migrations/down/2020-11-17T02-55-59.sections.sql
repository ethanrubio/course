--sections (down)
DROP TRIGGER IF EXISTS set_sections_timestamp ON sections;

DROP TABLE IF EXISTS sections;