--section-users (down)
DROP TRIGGER IF EXISTS set_section_users_timestamp ON section_users;

DROP TABLE IF EXISTS section_users;