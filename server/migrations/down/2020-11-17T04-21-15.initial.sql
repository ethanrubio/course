--initial (down)
DELETE FROM content_releases WHERE section_id IN (SELECT id FROM sections WHERE course_id IN (SELECT id FROM courses WHERE name IN ('The Physics of Time', 'How To Write a Book')));

DELETE FROM sections
WHERE course_id IN (SELECT id FROM courses WHERE name IN ('The Physics of Time', 'How To Write a Book'));

DELETE FROM sessions
WHERE course_id IN (SELECT id FROM courses WHERE name IN ('The Physics of Time', 'How To Write a Book'));

DELETE FROM courses
WHERE name IN ('The Physics of Time', 'How To Write a Book');

