--content-releases (up)
CREATE TABLE IF NOT EXISTS content_releases (
    id bigserial NOT NULL,
    section_id uuid NOT NULL,
    session_id uuid NOT NULL,
    release_date date NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT pk_section_id_session_id_release_date_id PRIMARY KEY (section_id, session_id, release_date)
);

CREATE TRIGGER set_content_releases_timestamp
    BEFORE UPDATE ON content_releases
    FOR EACH ROW
    EXECUTE PROCEDURE trigger_set_timestamp ();

