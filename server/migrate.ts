import { setupSlonikMigrator } from "@slonik/migrator";
import { slonik } from "./src/internal/db/db";

export const migrator = setupSlonikMigrator({
  migrationsPath: __dirname + "/migrations",
  slonik,
  mainModule: module,
});
