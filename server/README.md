# The Course App Server

## Getting Started
1. Add a `.env` file in this folder
    ```bash
    echo 'DATABASE=course-db
    DATABASE_USER=admin
    DATABASE_PASSWORD=password
    DATABASE_HOST=localhost
    DATABASE_PORT=5560
    SECRET=super_duper_secret' >> .env
    ```
1. Install the dependencies
    ```
    yarn
    ```
1. Start the db
    ```
    yarn run db:up
    ```
1. Run the migrations
    ```
    yarn run migrate:up
    ```
1. Run the server
    ```
    yarn start
    ```
1. Run the tests
    ```
    yarn test
    ```

## File Structure

```
├── migrations      # the migrations for the database
├── src             # the backend
│   ├── graphql     # the resolvers and the GraphQL schema
│   ├── internal    # auth, db, middleware
│   ├── managers    # business logic
│   ├── stores      # database logic
│   └── server.ts   # server entry point
└── tests           # tests
```

## Thoughts for Improvement

1. Creating logic for adding courses/sections/sections
    1. When a course is added a semester/quarter's worth of sections are added as well as populating the `content-releases`
    1. Creating a section will also automatically populate the `content-releases` table with the appropriate release dates based on the section's `date_start`
1. Creating a cron job that runs on a semester/quarterly basis that creates the necessary `sections` and `content-releases`
1. Searching for courses and sections
1. Adding pagination for courses when it becomes too many
1. Adding more unit tests
1. Adding integration tests for GraphQL and the database
1. Prevent users from signing up for the multiple sections of the same course