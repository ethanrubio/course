# Course App

> A basic app that allows you to sign up for courses that are offered.

![Course App Sample](./images/sample.png)

## Overview
- Each course has 4 sessions, and each session's content is released on a weekly basis.
- A new section of the course is opened for sign up every 2 weeks.
- There is a cap of 10 people per course section.
- All session titles and descriptions for a course should be visible to users before sign up.
- A session's content should be visible only by people who have signed up for the course.
- A list of users signed up for each course section should be visible.
- A user should be able to register for a course and remove themselves from the course.

### Getting Started

See the [server](./server/README.md) and [client](./course-client/README.md) setup for more instructions.